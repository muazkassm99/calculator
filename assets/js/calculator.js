//this var tells if the equal btn was pressed and determins all
//the operations depending on that
let equalized = false;


//a list that has the arithatics
let arith = []
//a list that has the final answers
let answer = []


//adding numbers to the arithmatic eqoution
$(".number").on("click", function(){
    if(equalized){
        $("#history").text("");
        $("#result").text($(this).text());
        equalized = false;
    }else{
        let oldTxt = $("#result").text();
        let newTxt = oldTxt + $(this).text();
        $("#result").text(newTxt);
    }
})


//clear all
$(".clear").on("click", function(){
    $("#result").text("");
    $("#history").text("");
    arith.length = 0;
    answer.length = 0;
    $(".inner-history").html("<div><head>History</head></div>");
})

//delete last character
$(".delete").on("click", function(){
    let txt = $("#result").text();
    txt = txt.substring(0, txt.length - 1);
    $("#result").text(txt);
})
//show history

//operation
$(".operation").on("click", function(){
    if($("#result").text()!==""){
        if(!equalized){
            $("#history").text($("#history").text() + " " + $("#result").text() + " " + $(this).text());
            $("#result").text("");
        }else{
            let l = $("#result").text().length;
            $("#history").text($("#result").text().substring(1, l) + " " + $(this).text());
            $("#result").text("");
            equalized = false;
        }
    }
})

//equal
$("#equal").on("click", function(){

    //append result to history
    if($("#result").text()[0] !== "="){
        let oldTxt = $("#history").text();
        let newTxt = $("#result").text();
        oldTxt = oldTxt + " " + newTxt;

        $("#history").text(oldTxt);
        $("#result").text( "= " + eval(oldTxt));
        equalized = true;

        if(!arith.includes(oldTxt)){
            arith.push(oldTxt);
            answer.push(eval(oldTxt).toString());
        }
    }
})


//display history
$("#his").on("click", function(){
    $(".calculator-container").toggleClass("shift-left");
    $(".history-container").toggleClass("show");

    fillHistory();
})

function fillHistory(){

    $(".inner-history").html("");
    $(".inner-history").html("<div><head>History</head></div>");
    //take from lists and put in div
    for (let index = 0; index < arith.length; index++) {
        console.log(arith[index] + "  = " + answer[index]);
        let ht = $(".inner-history").html();
        let newTxt = "<p>" + arith[index] + " = " + answer[index] + "</p>";
        $(".inner-history").html(ht + newTxt);
    }
}